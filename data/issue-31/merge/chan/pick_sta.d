#
 #                              MinBigZC       RawDataFilt    LtaFilt         DeadSta          PreEvent
# Pick  Pin    Station/   MinSmallZC   MaxMint           StaFilt       RmavFilt           AltCoda
# Flag  Numb   Comp/Net   Itr1   MinPeakSize  i9  CharFuncFilt  EventThresh          CodaTerm         Erefs
# ----  ----   --------   ----------------------------------------------------------------------------------
1  1 DSFO HHE OH 10 3  40  3 162  500  3  .939  3.  .4  .015 5.  .9961  1200. 132.7   .8  1.5 135000. 8388608
1  1 DSFO HHN OH 10 3  40  3 162  500  3  .939  3.  .4  .015 5.  .9961  1200. 132.7   .8  1.5 135000. 8388608
1  1 DSFO HHZ OH 10 3  40  3 162  500  3  .939  3.  .4  .015 5.  .9961  1200. 132.7   .8  1.5 135000. 8388608
# 2023-10-26 This is a comment! Added by ewmerge, part of ewconfig.
#
 #                              MinBigZC       RawDataFilt    LtaFilt         DeadSta          PreEvent
# Pick  Pin    Station/   MinSmallZC   MaxMint           StaFilt       RmavFilt           AltCoda
# Flag  Numb   Comp/Net   Itr1   MinPeakSize  i9  CharFuncFilt  EventThresh          CodaTerm         Erefs
# ----  ----   --------   ----------------------------------------------------------------------------------
1  1 HINO HHE OH 10 3  40  3 162  500  3  .939  3.  .4  .015 5.  .9961  1200. 132.7   .8  1.5 135000. 8388608
1  1 HINO HHN OH 10 3  40  3 162  500  3  .939  3.  .4  .015 5.  .9961  1200. 132.7   .8  1.5 135000. 8388608
1  1 HINO HHZ OH 10 3  40  3 162  500  3  .939  3.  .4  .015 5.  .9961  1200. 132.7   .8  1.5 135000. 8388608
#
 #                              MinBigZC       RawDataFilt    LtaFilt         DeadSta          PreEvent
# Pick  Pin    Station/   MinSmallZC   MaxMint           StaFilt       RmavFilt           AltCoda
# Flag  Numb   Comp/Net   Itr1   MinPeakSize  i9  CharFuncFilt  EventThresh          CodaTerm         Erefs
# ----  ----   --------   ----------------------------------------------------------------------------------
1  1 KIOH HHE OH 10 3  40  3 162  500  3  .939  3.  .4  .015 5.  .9961  1200. 132.7   .8  1.5 135000. 8388608
1  1 KIOH HHN OH 10 3  40  3 162  500  3  .939  3.  .4  .015 5.  .9961  1200. 132.7   .8  1.5 135000. 8388608
1  1 KIOH HHZ OH 10 3  40  3 162  500  3  .939  3.  .4  .015 5.  .9961  1200. 132.7   .8  1.5 135000. 8388608
