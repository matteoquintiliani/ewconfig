# Install

## Virtual Environment (Local)

    > cd INSTALL_DIR
    > python3.9 -m venv env
    > source env/bin/activate
    > pip install --upgrade pip
    > git clone https://gitlab.com/seismic-software/ewconfig.git
    > cd ewconfig/py
    > pip install .
    # optionally remove the source
    > cd ../..
    > rm -fr ewconfig

To use:

    > cd INSTALL_DIR
    > source env/bin/activate

## System Install (Global)

    > cd TMP_DIR
    > git clone https://gitlab.com/seismic-software/ewconfig.git
    > cd ewconfig/py
    > pip install .

# Updates

If you are developing the system and want to always execute the latest
code use

    > pip install -e .

above.  The `-e` flag will keep the install synchronized with the code
in the git directory.

Otherwise, if you want to update a previous installation, use

    > pip uninstall ewconfig

and then follow the previous install instructions.
