from logging import getLogger
from shutil import rmtree
from unittest import TestCase

from ewconfig.lib.file import tmp_dir
from ewconfig.merge.prefer import Prefer
from tests.lib.log import LogMixin
from tests.lib.merge import MergeMixin

log = getLogger(__name__)


class TestIssue31(MergeMixin, LogMixin, TestCase):

    stderr_verbosity = 5

    def test_merge_xml(self):
        merged = self.run_merge('issue-31',
                                ['OH-DSFO.xml', 'OH-HINO.xml', 'OH-KIOH.xml'],
                                lo_precision=True, merged=tmp_dir(), prefer=Prefer.OLD,
                                comment='This is a comment!')
        self.assert_files(merged, 'issue-31', subdir='merge')
        rmtree(merged)
