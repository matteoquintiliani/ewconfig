from glob import glob
from logging import getLogger
from os.path import join
from shutil import rmtree
from unittest import TestCase

from ewconfig.ewmerge import main
from ewconfig.lib.file import tmp_dir
from tests.lib.file import project_root
from tests.lib.log import LogMixin
from tests.lib.merge import MergeMixin

log = getLogger(__name__)


class TestNoOld(MergeMixin, LogMixin, TestCase):
    """
    tests for changing ewmerge so that --old is optional.
    """

    def __news(self):
        input = join(project_root(), 'data', 'no_old', 'news')
        # this order makes the sorted/no-sorted different
        return sorted(glob(join(input, '*.xml')), reverse=True)

    def test_no_old(self):
        merged = main(self.__news(), date='2022-01-01', merged=tmp_dir(), sort='NSCL')
        self.assert_files(merged, 'no_old', 'merge_no_old')
        rmtree(merged)

    def test_no_sort(self):
        merged = main(self.__news(), date='2022-01-01', sort=None, merged=tmp_dir())
        self.assert_files(merged, 'no_old', 'merge_no_sort')
        rmtree(merged)

    def test_old(self):
        merged = main(self.__news(), old=join(project_root(), 'data', 'no_old', 'old'),
                      date='2022-01-01', merged=tmp_dir(), sort='NSCL')
        self.assert_files(merged, 'no_old', 'merge_old')
        rmtree(merged)

    def test_old_no_sort(self):
        merged = main(self.__news(), old=join(project_root(), 'data', 'no_old', 'old'),
                      date='2022-01-01', sort=False, merged=tmp_dir())
        self.assert_files(merged, 'no_old', 'merge_old_no_sort')
        rmtree(merged)
