from filecmp import dircmp
from logging import getLogger
from os import rmdir
from re import search
from shutil import rmtree
from unittest import TestCase

from ewconfig.lib.file import tmp_dir
from ewconfig.lib.filter import Filter
from ewconfig.merge.prefer import Prefer
from tests.lib.log import LogMixin, CaptureLog
from tests.lib.merge import MergeMixin

log = getLogger(__name__)


class TestIssue30(MergeMixin, LogMixin, TestCase):

    stderr_verbosity = 5

    def test_merge_xml(self):
        merged = self.run_merge('issue-30',
                                ['OH-DSFO.xml'],
                                lo_precision=True, merged=tmp_dir(), prefer=Prefer.NEW, sort='SN')
        self.assert_files(merged, 'issue-30', subdir='merge')
        rmtree(merged)

