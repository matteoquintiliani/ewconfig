from filecmp import dircmp
from logging import getLogger
from os import rmdir
from re import search
from shutil import rmtree
from unittest import TestCase

from ewconfig.lib.file import tmp_dir
from ewconfig.lib.filter import Filter
from ewconfig.merge.prefer import Prefer
from tests.lib.log import LogMixin, CaptureLog
from tests.lib.merge import MergeMixin

log = getLogger(__name__)


class TestIssue32(MergeMixin, LogMixin, TestCase):

    stderr_verbosity = 4

    def test_merge_xml(self):
        with CaptureLog() as captured_log:
            merged = self.run_merge('issue-32', ['OH-DSFO.xml', 'OH-HINO.xml'],
                                    lo_precision=True, merged=tmp_dir(), prefer=Prefer.OLD, sort='NSCL')
        self.assert_files(merged, 'issue-32', subdir='merge')
        rmtree(merged)
        self.assertTrue(search('replacing \[NE8K  LM  HHE  42 47.4470', captured_log.out), captured_log.out)
