from logging import getLogger
from shutil import rmtree
from unittest import TestCase

from ewconfig.lib.file import tmp_dir
from ewconfig.merge.prefer import Prefer
from tests.lib.log import LogMixin
from tests.lib.merge import MergeMixin

log = getLogger(__name__)


class TestIssue28(MergeMixin, LogMixin, TestCase):

    stderr_verbosity = 5

    def test_merge_xml(self):
        # note that the sac files in the x2ew directory DO NOT match the ones in the merge directory
        # (they did when the bug existed)
        merged = self.run_merge('issue-28',
                                ['OH-DSFO.xml'],
                                lo_precision=True, merged=tmp_dir(), prefer=Prefer.NEW, sort='NSCL')
        self.assert_files(merged, 'issue-28', subdir='merge')
        rmtree(merged)

