from logging import getLogger
from os.path import join
from shutil import rmtree
from unittest import TestCase

from ewconfig.ewmerge import main
from ewconfig.lib.file import tmp_dir
from ewconfig.lib.write import NM_IN_M
from tests.lib.file import project_root
from tests.lib.log import LogMixin
from tests.lib.merge import MergeMixin

log = getLogger(__name__)


class TestIssue6(MergeMixin, LogMixin, TestCase):

    stderr_verbosity = 5

    def test_merge(self):
        merged = main([join(project_root(), 'data', 'issue-9', 'issue-9.xml')], date='2023-10-21',
                      sort=None, merged=tmp_dir(), m_to_nm=NM_IN_M, chans=['BH'])
        self.assert_files(merged, 'issue-9', 'merge')
        rmtree(merged)
