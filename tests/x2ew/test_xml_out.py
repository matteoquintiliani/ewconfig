from shutil import rmtree
from unittest import TestCase

from ewconfig.lib.filter import Filter
from tests.lib.log import LogMixin
from tests.lib.x2ew import MainMixin


class TestXmlOut(MainMixin, LogMixin, TestCase):

    stderr_verbosity = 5

    def test_station_xml(self):
        dest_dir = self.run_xml2ew('xml-out', chan_filter=Filter('Channel', excludes=['HHZ']), date='2015-01-01',
                                   xml_out='xml-out.xml')
        self.assert_files(dest_dir, 'xml-out')
        rmtree(dest_dir)
