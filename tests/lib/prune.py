from os.path import join

from tests.lib.compare import CompareMixin
from tests.lib.file import project_root


class PruneMixin(CompareMixin):

    def _src_dir(self, name):
        # use the conversion test targets as input
        return join(project_root(), 'data', name.lower(), 'x2ew')

    def run_prune(self, name, **kwargs):
        from ewconfig.ewprune import main
        dir = self._src_dir(name)
        return main(dir, **kwargs)

    def _test_dir(self, name, subdir):
        return join(project_root(), 'data', name.lower(), subdir)

    def assert_files(self, pruned, name, subdir='prune', skip_files=None, skip_lines=None,
                     extra_dirs=tuple()):
        test_dir = self._test_dir(name, subdir)
        self._assert_files(pruned, test_dir, skip_files=skip_files, skip_lines=skip_lines, extra_dirs=extra_dirs)
