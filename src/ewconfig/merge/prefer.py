from enum import Enum, auto


class Prefer(Enum):
    CHECK = auto()
    OLD = auto()
    NEW = auto()