#!/bin/bash

cd "${BASH_SOURCE%/*}/" || exit
cd ..
source env/bin/activate

PYTHONPATH=src python -m unittest $@ |& tee tests.log


