#!/bin/bash

PYTHON=python3.9

cd "${BASH_SOURCE%/*}/" || exit
cd ..

rm -fr env
$PYTHON -m venv env
source env/bin/activate

python -m pip install --upgrade pip build twine 
python -m pip install -e .

