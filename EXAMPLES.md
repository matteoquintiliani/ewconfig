
# Examples

## Create a config from a station.xml file

Create the config in the current directory

    > stationxml2ew station.xml

Create the config in the configdir directory

    > stationxml2ew station.xml --dir configdir

We can also use `ewmerge` (which calls `stationxml2ew` when needed to
expand the xml).  In this case we are "merging" a single file.

    > ewmerge --new station.xml --merged configdir

## Create a config from multiple station.xml files

Create and then merge the configs:

    > stationxml2ew station1.xml station2.xml --dir configdir1 configdir2
    > ewmerge --old configdir1 --new configdir2 --backup NONE

(we don't create a backup for configdir1 because we can always
recreate it)

Since `ewmerge` will call `stationxml2ew` automatically we can also do
this in a single step:

    > ewmerge --new station1.xml station2.xml --merged configdir1

## Merge a station.xml into an existing config

    > ewmerge --old configold --new station.xml --merged configboth

We could also do this in two steps, expanding the station.xml first:

    > stationxml2ew station.xml --dir confignew
    > ewmerge --old configold --new confignew --merged configboth

## Remove a station from an existing config

    > ewprune config --delete 'net.sta' 'net.sta.*.*'

(note that we need to remove both NS and NSCL entries)

## Remove a channel from an existing config

    > ewprune config --delete 'net.sta.chan.loc'

(note that we did not delete NS entries - presumably the station has
other channels).
