# stationxml2ew:
```
usage: stationxml2ew [-h] [--dir [DIR ...]] [--xml-out FILE [FILE ...]]
                     [--include-chan [PATTERN ...]]
                     [--exclude-chan [PATTERN ...]]
                     [--include-sta [PATTERN ...]]
                     [--exclude-sta [PATTERN ...]] [--date DATE]
                     [--pick-fp-sta STRING]
                     [--displacement | --velocity | --acceleration] [-c PRE]
                     [-n] [-g] [-V] [--md-help] [-v INTEGER]
                     FILE [FILE ...]

```
A scanner program to convert a station.xml file to configurations suitable for
use with localmag, hypoinverse, pick_ew, pick_FP, slink2ew, carlstatrig and
wave_serverV station lists. It is presumed that the input file is complete,
with lat/long/elevation for the SCNL. Output sac files will be in meters
unless the --nano parameter is set to convert to nanometers.

Stations and channels can be selected using --include-chan (-sta) and
--exclude-chan (-sta). These take patterns that can match any string (\*), any
character (?), and any character from a sequence ([abc]). If --include-chan
(-sta) is not given it is assumed to be "\*". Channels, for example, are
included if they match --include-chan and do not match --exclude-chan.

Specifying --date selects channels active at that time. By default this is the
current time.

Responses types are recognised via the response units. Unrecognised units can
be given a default using --displacement|--velocity|--acceleration. If no
default is given they are skipped. Pressure sensitive stations (units Pa) are
always skipped.

## Examples

To generate a config in the current directory from an XML file:

stationxml2ew station.xml

To generate a config in a different directory:

stationxml2ew station.xml --dir output/dir

To process multiple XML files, each with a different output directory:

stationxml2ew station1.xml station2.xml --dir station1 station2

To convert multiple XML files into a single directory see ewmerge (which can
call this program and then do the merge to a single directory).

## positional arguments:
### FILE
```
Station XML file(s)
```

## optional arguments:
### -h, --help
```
show this help message and exit
```

### --dir [DIR ...]
```
Output directory(s) (default CWD for a single input file)
```

### --xml-out FILE [FILE ...]
```
Filtered station.xml output
```

### --include-chan [PATTERN ...]
```
Pattern to match included channel names (default all).
```

### --exclude-chan [PATTERN ...]
```
Pattern to match excluded channel names.
```

### --include-sta [PATTERN ...]
```
Pattern to match included station names (default all).
```

### --exclude-sta [PATTERN ...]
```
Pattern to match excluded station names.
```

### --date DATE
```
Use channels valid on this date (default now).
```

### --pick-fp-sta STRING
```
Extra values for pick_FP_sta.d (default "-1 -1 10.0 10.0 -1").
```

### --displacement
```
Assume unrecognized responses are displacement.
```

### --velocity
```
Assume unrecognized responses are velocity.
```

### --acceleration
```
Assume unrecognized responses are acceleration.
```

### -c PRE, --chans PRE
```
Prefixes for picked channels (default HH,EH)
```

### -n, --nano
```
Convert Constant meters into nanometers. You must use this option if you are using the default Localmag or GMEW settings. If you have enabled the ResponseInMeters option in localmag.d and/or gmew.d, then you don't need this option.
```

### -g, --geophone
```
Treat ALL instruments as Geophones in pick_ew output. Otherwise broadband settings are used unless channel starts with E.
```

### -V, --version
```
Show program's version number and exit.
```

### --md-help
```
Print Markdown-formatted help text and exit.
```

### -v INTEGER, --verbosity INTEGER
```
Log level (0: silent, 5: debug)
```
