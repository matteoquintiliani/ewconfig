# sacpz2ew:
```
usage: sacpz2ew [-h] [-c PRE] [-d] [-n] [-g] [-V] [--md-help] [-v INTEGER]
                pzdirectory

```
A scanner program to take SAC PZ files from rdseed -pf and convert them to
configurations suitable for use with localmag, hypoinverse, pick_ew, pick_FP,
slink2ew, carlstatrig and wave_serverV station lists. It is presumed that the
dataless was complete and had lat/long/elevation for the SCNL. Output sac
files will be in meters unless the --nano parameter is set to convert to
nanometers. The output files will appear in the current directory.

## positional arguments:
### pzdirectory
```
Directory containing the input files -- which are the output of rdseed -pf
```

## optional arguments:
### -h, --help
```
show this help message and exit
```

### -c PRE, --chans PRE
```
Prefixes for picked channels (default HH,EH)
```

### -d, --drop-comment
```
Delete the comments from the sac output.
```

### -n, --nano
```
Convert Constant meters into nanometers. You must use this option if you are using the default Localmag or GMEW settings. If you have enabled the ResponseInMeters option in localmag.d and/or gmew.d, then you don't need this option.
```

### -g, --geophone
```
Treat ALL instruments as Geophones in pick_ew output. Otherwise broadband settings are used unless channel starts with E.
```

### -V, --version
```
Show program's version number and exit.
```

### --md-help
```
Print Markdown-formatted help text and exit.
```

### -v INTEGER, --verbosity INTEGER
```
Log level (0: silent, 5: debug)
```
